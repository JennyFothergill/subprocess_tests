# project.py
import time

from flow import FlowProject
from flow.environment import DefaultSlurmEnvironment


class Project(FlowProject):
    pass


class Fry(DefaultSlurmEnvironment):
    hostname_pattern = "fry"
    template = "fry.sh"


@Project.label
def greeted(job):
    return job.isfile('hello.txt')


@Project.operation
@Project.post(greeted)
def hello(job):
    time.sleep(3)
    with job:
        with open('hello.txt', 'w') as file:
            file.write('world!\n')


if __name__ == '__main__':
    Project().main()
